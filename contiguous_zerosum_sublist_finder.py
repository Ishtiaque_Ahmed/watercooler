def find_contiguous_sublists_having_particular_sum(main_list, desired_sum):
    _sum = 0
    cumulative_sums_list = [] # A list containing cumulative sums starting from 0th index
    cumulative_sums_map = {} # A map of cum_sum_value -> list of ending indices up to which we get that cum_sum_value
    for i, x in enumerate(main_list):
        _sum += x
        cumulative_sums_list.append(_sum)
        if _sum not in cumulative_sums_map:
            cumulative_sums_map[_sum] = []
        cumulative_sums_map[_sum].append(i)

    #print(cumulative_sums_list)
    #print(cumulative_sums_map, '\n')

    cumulative_sums_list = [0] + cumulative_sums_list # Prepending a 0 to make the upcoming for-loop consistent to the initial condition
    result = []
    for i, _sum in enumerate(cumulative_sums_list): # i is the starting index of our search
        sum_value_to_look_for = desired_sum + _sum # This is done because the cumulative list takes the sum from the beginning of the main_list
        if sum_value_to_look_for in cumulative_sums_map:
            for j in cumulative_sums_map[sum_value_to_look_for]: # j is the ending index
                if i < j:
                    result.append(main_list[i: j+1]) # the sublist is inclusive of the j-th element
    return result


if __name__ == "__main__":
    l = [3, 4, -7, 3, 1, 3, 1, -4, -2, -2]
    sum_val = 0

    result = find_contiguous_sublists_having_particular_sum(l, sum_val)
    for sublist in result:
        print(sublist)
